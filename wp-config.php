<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress_user');

/** MySQL database password */
define('DB_PASSWORD', 'farouche-hole-gawk-protocol');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'RfV}xs1_>M_[PKN5ws_;odfu6i&m lw[`g7(|CJT^s<MT_{8kQ(?zwMh:o#H`=W+');
define('SECURE_AUTH_KEY',  'X0~S]{Rh2Fs?|;4YUP$~7w:g;=>7<8FQx9|R=[,h)Ioy A.*&h,*{-FcQ uj!d77');
define('LOGGED_IN_KEY',    '~m5%zcMNfB$_WqtXcZZ^yl,++Se(>o)NqH2N15@B_DT)!?&;?B0AltfC YbjxI}4');
define('NONCE_KEY',        '.|+W34qjQ^1]Jlwj1 8B3]kNUzmXG[Y:/1K$=02g9bz73yKOK2FPIAXXyt2gf:Wl');
define('AUTH_SALT',        'Mx{[3NWf5DNo`UtWi52Ay._kp3@p<aeih-yBXpY)]:c6pQ1}Pbzw&4mDBL0Cr u_');
define('SECURE_AUTH_SALT', '0@=>:<dJQ:{0Hp3D:hWmsGzN|4k.WgzFX>}rU?iDU#jlZFZvV`MkBiB_tuD}<V!H');
define('LOGGED_IN_SALT',   'E=D%=n*qVb^zaxNkT3xLxxaY|:kgDzpVb*&U}13SNFv7/&_w)Wx9A,!:iC#J>T72');
define('NONCE_SALT',       '`h8Ka9OXsZa+f3CBNn*f,%{3MgNZiBS#mv CN=`1g6qb.pNGN*7[~ jR6V/HV&SE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
